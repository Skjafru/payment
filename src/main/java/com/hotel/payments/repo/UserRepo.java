package com.hotel.payments.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hotel.payments.entity.User;
//@Repository
public interface UserRepo extends JpaRepository<User, Integer> {
	
	
}


