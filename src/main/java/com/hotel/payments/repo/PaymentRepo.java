package com.hotel.payments.repo;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hotel.payments.entity.Payment;

@Repository
public interface PaymentRepo extends JpaRepository<Payment, Integer> {

	List<Payment> findAllByTransactionDateBetween(@Param("fromdate") Date TransactionDateStart,
			@Param("todate") Date TransactionDateEnd);
	
	
	
	//@Query("select p from Payment p where transactionDate(p.transactionDate) = transactionDate(?)")
	List<Payment> findByTransactionDate(Date transactionDate);//@Temporal(TemporalType.DATE) Date date
	
		
	
	
}
