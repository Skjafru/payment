package com.hotel.payments.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hotel.payments.entity.Role;

public interface RoleRepo extends JpaRepository<Role, Integer> {

}
