package com.hotel.payments.vo;

public class Response {

	private String message;
	private String purchaseAmount;
	private String remaningAmount;
	private String paidAmount;
	private String errorMessage;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getPurchaseAmount() {
		return purchaseAmount;
	}

	public void setPurchaseAmount(String purchaseAmount) {
		this.purchaseAmount = purchaseAmount;
	}

	public String getRemaningAmount() {
		return remaningAmount;
	}

	public void setRemaningAmount(String remaningAmount) {
		this.remaningAmount = remaningAmount;
	}

	public String getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(String paidAmount) {
		this.paidAmount = paidAmount;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public Response(String message, String purchaseAmount, String remaningAmount, String paidAmount,
			String errorMessage) {
		super();
		this.message = message;
		this.purchaseAmount = purchaseAmount;
		this.remaningAmount = remaningAmount;
		this.paidAmount = paidAmount;
		this.errorMessage = errorMessage;
	}

	public Response() {
		super();
	}

}