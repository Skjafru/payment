package com.hotel.payments.controller;


import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.hotel.payments.entity.Payment;
import com.hotel.payments.repo.PaymentRepo;

@RestController("/payments")
public class PaymentController {

	@Autowired
	PaymentRepo paymentRepo;
	
	@PostMapping("/savetransaction")
	@ResponseBody Payment saveTransaction(@RequestBody Payment payments) {
		
		payments.setTransactionDate(new Date());
		paymentRepo.save(payments);
		return payments;
		
	}

}
